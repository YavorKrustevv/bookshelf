/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.bookshelf;

import com.mysql.cj.jdbc.MysqlDataSource;
import static com.yavv.bookshelf.BookShelfUI.LOG;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author yavor
 */
public class DataSource { // Secure and convinient connection to database with clear logging.

    private AddBooks ref; // Reference for inserting books method.
    MysqlDataSource ds = getDataSource();

    public MysqlDataSource getDataSource() {                                    //  Method for Data Source object.

        Properties props = new Properties();                                    // SQL DB login details. Adds sequrity.

        String fileName = "src/main/resources/db.properties";                   // Location of the login details.

        try (FileInputStream fis = new FileInputStream(fileName)) {
            props.load(fis);
        } catch (IOException ex) {
            BookShelfUI.LOG.error("Creating data source error : ", ex);
        }

        MysqlDataSource ds = new MysqlDataSource();
        ds.setURL(props.getProperty("mysql.url"));
        ds.setUser(props.getProperty("mysql.username"));
        ds.setPassword(props.getProperty("mysql.password"));

        return ds;

    }

    public void getDatabase(String createDB) {

        try (Connection con = ds.getConnection();
                PreparedStatement pst = con.prepareStatement(createDB)) {
            pst.execute();

        } catch (SQLException ex) {
            BookShelfUI.LOG.error("Connecting with data source :", ex);
        }
    }

    public void insertNewBook() {                                               // Insert new book in SQL Database.

        SQLStatements statement = new SQLStatements();

        try (Connection con = ds.getConnection();
                PreparedStatement pst = con.prepareStatement(statement.getBookStatement())) {

            // Table 1 Books : Title, Author, Genre.
            pst.setString(1, ref.getBookTitle());                           // VALUE 1 Title.
            pst.setString(2, ref.getBookAuthor());                          // VALUE 2 Author.
            pst.setString(3, ref.genre);
            // VALUE 3 Genre.
            
            FileInputStream txtInputStream = new FileInputStream(ref.getSynopsisFile());           // VALUE 4 Read synopsis file.
            pst.setBinaryStream(4, txtInputStream, (int) ref.getSynopsisFile().length());     // Binary Stream
            
            FileInputStream imgInputStream = new FileInputStream(ref.getCoverFile());            // VALUE 5 Read cover file.
            pst.setBinaryStream(5, imgInputStream, (int) ref.getCoverFile().length());

            boolean result = pst.execute();
            // Confirmation.
            if (result == false) {
                JOptionPane.showMessageDialog(null, "Book added!");
            } else {
                JOptionPane.showMessageDialog(null, "Fail! Book not added!");
            }
            // End  confirmation.
        } catch (SQLException | FileNotFoundException ex) {
            Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // Fetches cover from database and displays it on book selection.
    public void getCover(String selected) {

        try (Connection con = new DataSource().getDataSource().getConnection();
                PreparedStatement pst = con.prepareStatement(new SQLStatements().getCoverPreviewStatement())) {
            
            pst.setString(1, selected);
            ResultSet rs = pst.executeQuery();
            
            if (rs.next()) {
                
                // Image file to be read and applied to COVER LABEL;
                File image = new File ("src/main/resources/temp/tempCover.jpg");
                
                // Getting the image file BLOB from MySQL Database;
                try (FileOutputStream fos = new FileOutputStream(image)) {
                    
                    Blob blob = rs.getBlob("Cover");
                    int len = (int) blob.length();
                    
                    byte[] bites = blob.getBytes(1, len);
                    fos.write(bites, 0, len);
                    
                } catch (IOException ex) {
                    Logger.getLogger(Preview.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Preview.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Fetches synopsis file from SQL DB and displays it. 
    public void getSynopsis(String selected) {

        try (Connection con = new DataSource().getDataSource().getConnection();
                PreparedStatement pst = con.prepareStatement(new SQLStatements().getSynopsisStatement())) {

            pst.setString(1, selected);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {

                File image = new File ("src/main/resources/temp/tempSynopsis.txt");

                try (FileOutputStream fos = new FileOutputStream(image)) {

                    Blob txt = rs.getBlob("Synopsis");
                    int txtLenhtx = (int) txt.length();
                    byte[] bites = txt.getBytes(1, txtLenhtx);
                    fos.write(bites, 0, txtLenhtx);

                } catch (IOException ex) {
                    Logger.getLogger(Preview.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Preview.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setAddBooksReference(AddBooks ref) {                            // Reference to AddBooks object to get data.
        this.ref = ref;
    }
}
