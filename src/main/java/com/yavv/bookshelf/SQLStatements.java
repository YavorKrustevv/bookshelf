/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.bookshelf;

/**
 *
 * @author yavor
 */
public class SQLStatements {

    private final String createDB = "CREATE DATABASE IF NOT EXISTS Bookshelf;"
            + "USE Bookshelf;"
            + "CREATE TABLE IF NOT EXISTS Books "
            + "(Id TINYINT PRIMARY KEY AUTO_INCREMENT, "
            + "Title VARCHAR(40), "
            + "Author VARCHAR(30), "
            + "Genre CHAR(10), "
            + "Synopsis BLOB, "
            + "Cover MEDIUMBLOB)",
            newBook = "USE Bookshelf;"
            + "INSERT INTO Books (Title,Author,Genre,Synopsis,Cover) VALUES(?,?,?,?,?)",
            getAuthors = "USE Bookshelf;"
            + "SELECT Author FROM Books",
            getGenres = "USE Bookshelf;"
            + "SELECT Genre FROM Books",
            getAllTitles = "USE Bookshelf;"
            + "SELECT * FROM Books",
            getAllGenres = "SELECT Genre FROM Books",
            getAuthorsByGenre = "SELECT Author FROM Books WHERE Genre = ?",
            getTitlesByAuthors = "SELECT Title FROM Books WHERE Author = ?",
            deleteBook = "DELETE FROM Books WHERE Title = ?",
            getBookCover = "SELECT Cover FROM Books WHERE Title = ?",
            getBookInfo = "SELECT * FROM Books WHERE Title = ?",
            getCover = "SELECT Cover FROM Books WHERE Title = ?",
            getSynopsis = "SELECT Synopsis FROM Books WHERE Title = ?";

    public String getDatabaseStatement() {
        return createDB;
    }

    public String getBookStatement() {
        return newBook;
    }

    public String getAuthorsStatement() {
        return getAuthors;
    }

    public String getGenresStatement() {
        return getGenres;
    }

    public String getAllTitlesStatement() {
        return getAllTitles;
    }

    public String getAllGenresStatement() {
        return getAllGenres;
    }

    public String getAuthorsByGenreStatement() {
        return getAuthorsByGenre;
    }

    public String getTitlesByAuthorsStatement() {
        return getTitlesByAuthors;
    }

    public String getDeleteBookStatement() {
        return deleteBook;
    }

    public String getBookCoverStatement() {
        return getBookCover;
    }
    public String getBookInfoStatement() {
        return getBookInfo;
    }
    public String getCoverPreviewStatement() {
        return getCover;
    }
    public String getSynopsisStatement() {
        return getSynopsis;
    }

}
