/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.bookshelf;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author yavor
 */
public class RemoveBook implements ActionListener {

    private DataSource data = new DataSource();

    private JFrame removeBookFrame = new JFrame("Remove Book");
    private JPanel combos = new JPanel(new GridLayout(3, 0)),
            bottonsPnl = new JPanel(new GridLayout(2, 0)),
            menuPnl = new JPanel(new GridLayout(3, 0)),
            removeBookMainPnl = new JPanel(new BorderLayout());
    private JComboBox pickAuthor = new JComboBox(),
            pickGenre = new JComboBox(),
            pickTitle = new JComboBox();
    private JLabel coverLbl = new JLabel();
    private JButton removeBook = new JButton("Delete Book"),
            close = new JButton("Close");
    private JTextPane resume = new JTextPane();
    private Set<String> listGenres = new LinkedHashSet<>();                         // Preserve insertion order.
    private Set<String> listAuthors = new LinkedHashSet<>();

    RemoveBook() {
        // Cover label.
        coverLbl.setPreferredSize(new Dimension(200, 300));
        menuPnl.setPreferredSize(new Dimension(200, 300));
        // Search menu.
        combos.add(pickGenre, new GridLayout(1, 0));
        combos.add(pickAuthor, new GridLayout(2, 0));
        combos.add(pickTitle, new GridLayout(3, 0));
        menuPnl.add(combos, new GridLayout(1, 0));

        resume.setPreferredSize(new Dimension(200, 100));
        menuPnl.add(resume, new GridLayout(2, 0));

        bottonsPnl.add(removeBook, new GridLayout(3, 0));
        bottonsPnl.add(close, new GridLayout(4, 0));
        menuPnl.add(bottonsPnl, new GridLayout(3, 0));
        menuPnl.setBorder(new EmptyBorder(10, 10, 10, 10));

        removeBookMainPnl.add(coverLbl, BorderLayout.WEST);
        removeBookMainPnl.add(menuPnl, BorderLayout.EAST);

        removeBookFrame.add(removeBookMainPnl);
        removeBookFrame.setResizable(false);
        removeBookFrame.pack();
        removeBookFrame.setLocationRelativeTo(null);

        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeBookFrame.dispose();
            }
        });

        removeBook.addActionListener(new DeleteBook());
        pickGenre.addItemListener(new GenreListenerForAuthors());
        pickAuthor.addItemListener(new AuthorsListenerForTitles());
        pickTitle.addItemListener(new TitlesListenerForCovers());
        getGenres();                                                            // Fills up genre combobox with all the present genres.
        noCover();
    }

    public void noCover() {

        try {
            BufferedImage img = ImageIO.read(new File("src/main/resources/covers/no-cover.jpeg"));
            Image picRes = img.getScaledInstance(200, 300, Image.SCALE_SMOOTH);
            ImageIcon icon = new ImageIcon(picRes);
            coverLbl.setIcon(icon);
        } catch (IOException ex) {
            Logger.getLogger(RemoveBook.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getGenres() {

        String genre;

        try (Connection con = data.getDataSource().getConnection();
                PreparedStatement pst = con.prepareStatement(new SQLStatements().getAllGenresStatement())) {

            listGenres.clear();
            pickGenre.removeAllItems();

            ResultSet rs = pst.executeQuery();
            listGenres.add("- Genre");

            while (rs.next()) {
                genre = rs.getString("Genre");
                listGenres.add(genre);
            }
            for (String str : listGenres) {
                pickGenre.addItem(str);
            }
        } catch (SQLException ex) {
            BookShelfUI.LOG.error("Can't retrive current genres!", ex);
        }
    }

    class GenreListenerForAuthors implements ItemListener { // Works, but need USE Database; ExecuteQuery doesn's support it.

        @Override
        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {

                listAuthors.clear();
                pickAuthor.removeAllItems();
                String selection = pickGenre.getSelectedItem().toString();

                try (Connection con = data.getDataSource().getConnection();
                        PreparedStatement pst = con.prepareStatement(new SQLStatements().getAuthorsByGenreStatement())) {

                    pst.setString(1, selection);
                    ResultSet rs = pst.executeQuery();
                    listAuthors.add("- Authors");

                    while (rs.next()) {
                        String author = rs.getString("Author");
                        listAuthors.add(author);
                    }
                    for (String str : listAuthors) {
                        pickAuthor.addItem(str);
                    }

                } catch (SQLException ex) {
                    BookShelfUI.LOG.error("Can't retrieve authors!", ex);
                }
            }
        }
    }

    class AuthorsListenerForTitles implements ItemListener {

        List<String> titlesList = new ArrayList<>();                            // Get all titles, even duplicates.

        @Override
        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {

                titlesList.clear();
                pickTitle.removeAllItems();
                String authorSelected = pickAuthor.getSelectedItem().toString();

                try (Connection con = data.getDataSource().getConnection();
                        PreparedStatement pst = con.prepareStatement(new SQLStatements().getTitlesByAuthorsStatement())) {

                    pst.setString(1, authorSelected);
                    ResultSet rs = pst.executeQuery();
                    titlesList.add("- Titles");

                    while (rs.next()) {
                        String title = rs.getString("Title");
                        titlesList.add(title);
                    }
                    for (String str : titlesList) {
                        pickTitle.addItem(str);
                    }

                } catch (SQLException ex) {
                    BookShelfUI.LOG.error("Can't retrive titles!", ex);
                }
            }
        }

    }

    class TitlesListenerForCovers implements ItemListener {

        @Override
        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {

                String titleCover = pickTitle.getSelectedItem().toString();

                try (Connection con = data.getDataSource().getConnection();
                        PreparedStatement pst = con.prepareStatement(new SQLStatements().getBookCoverStatement())) {

                    pst.setString(1, titleCover);
                    ResultSet rs = pst.executeQuery();

                    if (rs.next()) {
                        // Cover image of the db.
                        FileOutputStream fos = new FileOutputStream("src/main/resources/temp/tempCover.jpg");

                        Blob blob = rs.getBlob("Cover");
                        int lengtx = (int) blob.length();   // Length of blob image in bytes.
                        byte cover[] = blob.getBytes(1, lengtx); // Image #1 blob bytes in array.
                        fos.write(cover);
                        // Loading cover image of the book for removal.
                        BufferedImage img = null;
                        try {
                            img = ImageIO.read(new File("src/main/resources/temp/tempCover.jpg"));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        Image resized = img.getScaledInstance(200, 300, Image.SCALE_SMOOTH);
                        coverLbl.setIcon(new ImageIcon(resized));

                    }

                } catch (SQLException | IOException ex) {
                    Logger.getLogger(RemoveBook.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    class DeleteBook implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String delete = pickTitle.getSelectedItem().toString();
            try (Connection con = data.getDataSource().getConnection();
                    PreparedStatement pst = con.prepareStatement(new SQLStatements().getDeleteBookStatement())) {

                pst.setString(1, delete);
                int deletion = pst.executeUpdate();

                if (deletion > 0) {
                    JOptionPane.showMessageDialog(menuPnl, "Book deleted!");
                } else {
                    JOptionPane.showMessageDialog(menuPnl, "Error deleting book!");
                }
            } catch (SQLException ex) {
                BookShelfUI.LOG.error("Error deleting book!", ex);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        removeBookFrame.setVisible(true);
    }
}
