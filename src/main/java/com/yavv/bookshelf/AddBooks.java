/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.bookshelf;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author yavor
 */
public class AddBooks implements ActionListener {

    private JFrame addBookFrame = new JFrame("Add Book");
    private JPanel addBookMenu = new JPanel(new GridLayout(7, 0)),
            addBookMainPnl = new JPanel(new BorderLayout());
    private JTextField addTitleField = new JTextField(15),
            addAuthorField = new JTextField(15);
    private JLabel noCover = new JLabel();
    private final ImageIcon noCoverImg = new ImageIcon("src/main/resources/covers/no-cover.jpeg");
    private JButton addCover = new JButton("Add Cover"),
            addSynopsis = new JButton("Add Synopsis"),
            addBookBtn = new JButton("Add Book"),
            close = new JButton("Close");
    protected String genre, authorTxtField, titleTxtField;
    private File cover, synopsis;
    private String[] genres = new String[]{"--", "History", "Biography", "Horror",
        "Adventure", "Romance", "Sci-Fi", "Mystery", "Crime"};                  // To be accessed class wide.
    private JComboBox pickGenre = new JComboBox(genres);

    AddBooks() {
        noCover.setIcon(noCoverImg);
        noCover.setPreferredSize(new Dimension(200, 300));
        addBookMenu.add(pickGenre, new GridLayout(1, 0));
        addTitleField.setToolTipText("Enter book title");
        addBookMenu.add(addTitleField, new GridLayout(2, 0));
        addAuthorField.setToolTipText("Enter book author");
        addBookMenu.add(addAuthorField, new GridLayout(3, 0));
        addBookMenu.add(addCover, new GridLayout(4, 0));
        addBookMenu.add(addSynopsis, new GridLayout(5, 0));
        addBookMenu.add(addBookBtn, new GridLayout(6, 0));
        addBookMenu.add(close, new GridLayout(7, 0));
        addBookMenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        addBookMainPnl.add(noCover, BorderLayout.WEST);
        addBookMainPnl.add(addBookMenu, BorderLayout.EAST);
        addBookFrame.add(addBookMainPnl);
        addBookFrame.setResizable(false);
        addBookFrame.pack();
        addBookFrame.setLocationRelativeTo(null);

        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addBookFrame.dispose();
            }
        });
        addCover.addActionListener(new ChooseCover());
        addSynopsis.addActionListener(new ChooseSynopsis());
        addBookBtn.addActionListener(new AddBookButton());
        pickGenre.addActionListener(new ClearFields());

        addTitleField.getDocument().addDocumentListener(new DocumentListener() {// Get book title.
            @Override
            public void insertUpdate(DocumentEvent e) {
                titleTxtField = addTitleField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                titleTxtField = addTitleField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                titleTxtField = addTitleField.getText();
            }
        });
        addAuthorField.getDocument().addDocumentListener(new DocumentListener() {// Get author name.
            @Override
            public void insertUpdate(DocumentEvent e) {
                authorTxtField = addAuthorField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                authorTxtField = addAuthorField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                authorTxtField = addAuthorField.getText();
            }
        });
    }

    class ChooseCover implements ActionListener {                               // Listener for picture choosing.

        final JFileChooser fcPic = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("Only images", "jpeg", "jpg", "png");

        ChooseCover() {
            cover = new File("src/main/resources/covers/no-cover.jpeg");
            noCover();
        }

        public void noCover() {

            try {
                BufferedImage img = ImageIO.read(new File("src/main/resources/covers/no-cover.jpeg"));
                Image picRes = img.getScaledInstance(200, 300, Image.SCALE_SMOOTH);
                ImageIcon icon = new ImageIcon(picRes);
                noCover.setIcon(icon);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(RemoveBook.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            fcPic.setFileFilter(filter);
            int returnVal = fcPic.showOpenDialog(addBookFrame);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                cover = fcPic.getSelectedFile();
                // Display chose file in a resized instance.
                try {
                    BufferedImage img = ImageIO.read(cover);
                    Image picRes = img.getScaledInstance(200, 300, Image.SCALE_SMOOTH);
                    ImageIcon icon = new ImageIcon(picRes);
                    noCover.setIcon(icon);
                    noCover.revalidate();

                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(RemoveBook.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            System.out.println("cover - " + cover);

        }
    }

    class ChooseSynopsis implements ActionListener {                            // Listener for book's text synopsis.

        final JFileChooser fcText = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("Only txt files", "txt");

        ChooseSynopsis() {
            synopsis = new File("src/main/resources/TXTs/no-synopsis.txt");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            fcText.setFileFilter(filter);
            int returnVal = fcText.showOpenDialog(addBookFrame);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                synopsis = fcText.getSelectedFile();                            // Synopsis File object.
            }
            System.out.println("synopsis - " + synopsis);

        }
    }

    class ClearFields implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            addAuthorField.setText("");
            addAuthorField.repaint();
            addTitleField.setText("");
            addTitleField.repaint();
        }
    }

    class AddBookButton implements ActionListener {                             // Adds all the selections to db.

        @Override
        public void actionPerformed(ActionEvent ad) {
            genre = pickGenre.getSelectedItem().toString();
            System.out.println("genre :" + genre + " author :" + authorTxtField + " title :" + titleTxtField);
            System.out.println("synopsis" + synopsis);
            System.out.println("cover" + cover);
            DataSource src = new DataSource();                                  // Calling the insert method from datasource class.
            src.setAddBooksReference(AddBooks.this);
            src.insertNewBook();
        }
    }

    @Override                                                                   // Set AddBook MenuItem Visible on click.
    public void actionPerformed(ActionEvent e) {
        addBookFrame.setVisible(true);
    }

    // Getters for all the field needed for the SQL Connection.
    public String getBookTitle() {
        return titleTxtField;
    }

    public String getBookAuthor() {
        return authorTxtField;
    }

    public String getGenre() {
        return genre;
    }

    public File getCoverFile() {
        return cover;
    }

    public File getSynopsisFile() {
        return synopsis;
    }
}
