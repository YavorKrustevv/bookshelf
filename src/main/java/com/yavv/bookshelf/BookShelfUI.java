/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.bookshelf;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yavor
 */
public class BookShelfUI extends JFrame {

    static final Logger LOG = LoggerFactory.getLogger(DataSource.class);
    
    private JFrame frame = new JFrame("Bookshelf");
    private JPanel mainPnl = new JPanel(new BorderLayout()), // Main panel for picking-title panel and make Booklists panel.
            coverAndsynopsis = new JPanel(new BorderLayout()), // Main panel for covers,synopsis,choosing titles.
            //listCombos = new JPanel(new BorderLayout()), // Panel for drop down lists of: booklists and authors in a chosen list.
            bookListandBtnPnl = new JPanel(),
            SynopsisPnl = new JPanel(), // Panel for book synopsis on a JTextPane.
            coverPnl = new JPanel(new BorderLayout()), // Panel for book covers/jpegs.
            readBookPnl = new JPanel(new BorderLayout()), // Bottom panel for "read" checkbox.
            sortPnl = new JPanel(new BorderLayout()), // Panel for sorting params.
            sortParamsComboPnl = new JPanel(new GridLayout(2, 2)), // Panel for sorting drop down menus : ByGenre, ByAuthor.
            addTitlePnl = new JPanel(), // Panel for add title MenuItem.
            sortButtonsPnl = new JPanel(new GridLayout(0, 2)),
            controls = new JPanel(new BorderLayout());
    // Menus.
    private JMenuBar menubar = new JMenuBar();
    private JMenu file = new JMenu("File"),
            titles = new JMenu("Books");                                        // Menu add/remove books.
    private JMenuItem exit = new JMenuItem("Exit"),
            addTitles = new JMenuItem("Add Books"), // add title.
            removeTitles = new JMenuItem("Remove Books");                       // Remove title.
    // Choosing a book.
    protected JComboBox bookListsCombo = new JComboBox(),
            // Sort panel.
            sortByGenre = new JComboBox(),
            sortByAuthor = new JComboBox(),
            sortTitles = new JComboBox();
    private JCheckBox readBook = new JCheckBox("Read already?");                // Tick to mark book as read.
    protected JLabel coverLabel = new JLabel();                                   // Label for displaying the book covers.                                          
    // Sort parameters.
    private JButton addToBookListBtn = new JButton("+ Selection to booklist"), // Adds sorted list to bookList with custom name.
            addBook = new JButton("Add title"),
            clear = new JButton("Clear");
    protected JTextPane bookSynopsisArea = new JTextPane();                       // Pane to display the selected title's resume.
    DataSource data = new DataSource();
    List<List<String>> toTable = new ArrayList<List<String>>();
    JTable table = new JTable();
    JScrollPane pane = new JScrollPane(table);

    BookShelfUI() {
        // Controls for sorting and selecting books.
        sortButtonsPnl.add(addBook, new GridLayout(0, 1));
        sortButtonsPnl.add(clear, new GridLayout(0, 2));
        sortParamsComboPnl.add(sortByGenre, new GridLayout(0, 1));
        sortParamsComboPnl.add(sortByAuthor, new GridLayout(0, 2));
        sortParamsComboPnl.add(sortTitles, new GridLayout(2, 1));
        sortParamsComboPnl.add(sortButtonsPnl, new GridLayout(2, 2));
        controls.add(sortParamsComboPnl, BorderLayout.NORTH);
        // Add to booklist combo and button panel.
        bookListandBtnPnl.add(bookListsCombo, BorderLayout.WEST);
        bookListandBtnPnl.add(addToBookListBtn, BorderLayout.CENTER);
        controls.add(bookListandBtnPnl, BorderLayout.SOUTH);
        // JTable with selected books.
        table.setFillsViewportHeight(true);
        pane.setPreferredSize(new Dimension(250, 150));
        controls.add(pane, BorderLayout.CENTER);
        // Adding the controls to the main panel.
        coverAndsynopsis.add(controls, BorderLayout.NORTH);
        // Book cover and synopsis.
        coverLabel.setPreferredSize(new Dimension(200, 300));
        coverPnl.add(coverLabel, BorderLayout.CENTER);
        coverAndsynopsis.add(coverPnl, BorderLayout.WEST);
        bookSynopsisArea.setPreferredSize(new Dimension(320, 300));
        coverAndsynopsis.add(new JScrollPane(bookSynopsisArea), BorderLayout.EAST);
        // Mark this book as already read.
        readBookPnl.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
        readBookPnl.add(readBook, BorderLayout.WEST);
        coverAndsynopsis.add(readBookPnl, BorderLayout.SOUTH);
        mainPnl.add(coverAndsynopsis, BorderLayout.CENTER);
        // Menus   
        menubar.add(file);
        menubar.add(titles);
        // Titles menu.
        file.add(exit);
        // Menu item in file menu.
        titles.add(addTitles);                                                  // Add new book.
        titles.add(removeTitles);                                               // Remove added book.
        mainPnl.add(menubar, BorderLayout.NORTH);                               // Menubar to the main panel.
        // Add sorted titles to book list window.
        frame.add(mainPnl);
        frame.setResizable(false);
        frame.pack();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        // Add the listeners.
        addTitles.addActionListener(new AddBooks());
        removeTitles.addActionListener(new RemoveBook());
        sortByGenre.addItemListener(new GenreListenerForAuthors());
        sortByAuthor.addItemListener(new AuthorsListenerForTitles());
        sortTitles.addItemListener(new BookPreview());
        addBook.addActionListener(new AddToList());
        addToBookListBtn.addActionListener(new MakeBookLists());
        bookListsCombo.addItemListener(new ReadyBookLists());
        exit.addActionListener(new ExitMenu());

        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toTable.clear(); // Clears previous selections.
                table.setModel(new DefaultTableModel()); // Displays empty table.
            }
        });
        AllGenreTitles(); // Load all genres at startup.
        noCover();
    }
    // SAME CODE IN REMOVE BOOKS!!!
    public void noCover() {

        try {
            BufferedImage img = ImageIO.read(new File("src/main/resources/covers/no-cover.jpeg"));
            Image picRes = img.getScaledInstance(200, 300, Image.SCALE_SMOOTH);
            ImageIcon icon = new ImageIcon(picRes);
            coverLabel.setIcon(icon);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(RemoveBook.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // Loads all genres that are currently in the database to the Genres combobox.
    public void AllGenreTitles() {

        String genre;
        Set<String> genres = new LinkedHashSet<>();

        try (Connection con = data.getDataSource().getConnection();
                PreparedStatement pst = con.prepareStatement(new SQLStatements().getAllGenresStatement())) {

            sortByGenre.removeAllItems();

            ResultSet rs = pst.executeQuery();
            genres.add("- Genre");

            while (rs.next()) {
                genre = rs.getString("Genre");
                genres.add(genre);
            }
            for (String str : genres) {
                sortByGenre.addItem(str);
            }
        } catch (SQLException ex) {
            LOG.error("Can't retrive current genres!", ex);
        }
    }

    // Loads all the authors in all the genres, shows authors in selected genre.
    class GenreListenerForAuthors implements ItemListener {                     // Works, but needs USE Database; ExecuteQuery doesn's support it.

        Set<String> authors = new LinkedHashSet<>();

        @Override
        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {

                authors.clear();
                sortByAuthor.removeAllItems();
                String selection = sortByGenre.getSelectedItem().toString();

                try (Connection con = data.getDataSource().getConnection();
                        PreparedStatement pst = con.prepareStatement(new SQLStatements().getAuthorsByGenreStatement())) {

                    pst.setString(1, selection);
                    ResultSet rs = pst.executeQuery();
                    authors.add("- Authors");

                    while (rs.next()) {
                        String author = rs.getString("Author");
                        authors.add(author);
                    }
                    for (String str : authors) {
                        sortByAuthor.addItem(str);
                    }

                } catch (SQLException ex) {
                    LOG.error("Can't retrieve authors!", ex);
                }
            }
        }
    }

    // Listens in the authors combobox and displays all the author's titles.
    class AuthorsListenerForTitles implements ItemListener {

        List<String> titles = new ArrayList<>(); // Collects all titles, even duplicates.

        @Override
        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {

                titles.clear();
                sortTitles.removeAllItems();
                String authorSelected = sortByAuthor.getSelectedItem().toString();

                try (Connection con = data.getDataSource().getConnection();
                        PreparedStatement pst = con.prepareStatement(new SQLStatements().getTitlesByAuthorsStatement())) {

                    pst.setString(1, authorSelected);
                    ResultSet rs = pst.executeQuery();
                    titles.add("- Titles");

                    while (rs.next()) {
                        String title = rs.getString("Title");
                        titles.add(title);
                    }
                    for (String str : titles) {
                        sortTitles.addItem(str);
                    }
                } catch (SQLException ex) {
                    LOG.error("Can't retrive titles!", ex);
                }
            }
        }

    }

    // Button listener for add book button.Adds selected title into the JTable.
    class AddToList implements ActionListener {

        String[] columns = {"#", "Genre", "Title", "Author"};
        String[][] details;
        String title, author, genre, num;
        int count;

        @Override
        public void actionPerformed(ActionEvent e) {

            String selectedTitle = sortTitles.getSelectedItem().toString();

            try (Connection con = data.getDataSource().getConnection();
                    PreparedStatement pst = con.prepareStatement(new SQLStatements().getBookInfoStatement())) {

                pst.setString(1, selectedTitle);
                ResultSet rs = pst.executeQuery();

                while (rs.next()) {
                    count++;
                    title = rs.getString(2);
                    author = rs.getString(3);
                    genre = rs.getString(4);
                    num = String.valueOf(count);
                }
                toTable.add(Arrays.asList(num, genre, title, author));
                prepareArrays(); // Transfer linked hash list to ordinary arrays.
                placeTable(details, columns); // Get data in JTable, display on screen.

            } catch (SQLException ex) {
                LOG.error("Display titles", ex);
            }
        }

        // Transfers titles from Set Collection to 2D arrays for the JTable.
        public void prepareArrays() {

            details = new String[toTable.size()][];
            int i = 0;
            for (List<String> str : toTable) {
                details[i++] = str.toArray(new String[str.size()]);
            }
        }

        // Displays JTable on the pane with custom Model and settings.
        public void placeTable(String[][] data, String[] cols) {
            table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
            table.setModel(new CustomTableModel(data, cols));
            // Set columns to specific width.
            TableColumnModel colModel = table.getColumnModel();
            colModel.getColumn(0).setMaxWidth(25);
            colModel.getColumn(1).setMaxWidth(80);
            colModel.getColumn(2).setMinWidth(200);
            // Center text in columns number and genre.
            DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
            renderer.setHorizontalAlignment(JLabel.CENTER);
            table.getColumnModel().getColumn(0).setCellRenderer(renderer);
            table.getColumnModel().getColumn(1).setCellRenderer(renderer);
            table.getColumnModel().getColumn(2).setCellRenderer(renderer);
        }

        // Custom model for the JTable.
        class CustomTableModel extends AbstractTableModel {

            CustomTableModel(String[][] data, String[] cols) {

            }

            @Override
            public int getColumnCount() {
                return columns.length;
            }

            @Override
            public int getRowCount() {
                return details.length;
            }

            // Columns headers titles.
            @Override
            public String getColumnName(int col) {
                return columns[col];
            }

            @Override
            public Object getValueAt(int row, int col) {
                return details[row][col];
            }
        }
    }

    // Listener class for cover and synopsis.
    class BookPreview implements ItemListener {

        @Override
        public void itemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) {

                // Loading book cover to the covel label.
                if (sortTitles.getSelectedIndex() > 0) {
                    String selected = sortTitles.getSelectedItem().toString();
                    System.out.println("selected : " + selected);
                    new DataSource().getCover(selected); // Getting the cover from db.
                    try {
                        BufferedImage img = ImageIO.read(new File("src/main/resources/temp/tempCover.jpg"));
                        // Resize to desired JLabel cover size - 200X300;
                        Image resized = img.getScaledInstance(200, 300, Image.SCALE_SMOOTH);
                        ImageIcon coverPreview = new ImageIcon(resized);
                        coverLabel.setIcon(coverPreview);
                    } catch (IOException ex) {
                        java.util.logging.Logger.getLogger(BookShelfUI.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    // Loading the book synopsis to the text area.
                    new DataSource().getSynopsis(selected); // Getting the synopis txt file from db.

                    try {
                        BufferedReader buffRead = new BufferedReader(new FileReader("src/main/resources/temp/tempSynopsis.txt"));
                        //bookSynopsisArea.setLineWrap(true);
                        //bookSynopsisArea.setWrapStyleWord(true);
                        bookSynopsisArea.read(buffRead, "synopsis stream");

                    } catch (IOException ex) {
                        java.util.logging.Logger.getLogger(BookShelfUI.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        }
    }

    class MakeBookLists implements ActionListener {

        String input;

        MakeBookLists() {
            bookListsCombo.addItem("- Choose booklist");
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            input = JOptionPane.showInputDialog(null, "Enter list name", "Enter text", JOptionPane.QUESTION_MESSAGE);
            if (input == null) {
                JOptionPane.showMessageDialog(null, "NO NAME ENTERED!");
            } else {
                try {
                    BufferedWriter buffer
                            = new BufferedWriter(new FileWriter(new File("src/main/resources/temp/lists/" + input + ".txt")));
                    // Write columns.
                    for (int i = 0; i < table.getColumnCount(); i++) {
                        buffer.write(table.getColumnName(i));
                        buffer.write("\t");
                    }
                    // Write rows with info.
                    for (int i = 0; i < table.getRowCount(); i++) {
                        buffer.newLine();
                        for (int j = 0; j < table.getColumnCount(); j++) {
                            buffer.write((String) (table.getValueAt(i, j)));
                            buffer.write("\t");
                        }
                    }
                    buffer.close();

                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(BookShelfUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    class ReadyBookLists implements ItemListener {

        String selection;
        String[] columns = {"", "", "", ""};

        ReadyBookLists() {
            File[] lists = new File("src/main/resources/temp/lists").listFiles();
            for (File str : lists) {
                if (str.isFile()) {
                    bookListsCombo.addItem(str.getName().replace(".txt", ""));  // Removes file extension.
                }
            }
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                if (bookListsCombo.getSelectedIndex() == 0) {                    // Clears pane if no selection.
                    table.setModel(new DefaultTableModel());
                }
                if (bookListsCombo.getSelectedIndex() > 0) {
                    table.setModel(new DefaultTableModel());
                    selection = bookListsCombo.getSelectedItem().toString(); // Get the book list name/txt file name.
                    String line = null;
                    DefaultTableModel txtModel = (DefaultTableModel) table.getModel(); // Model with the txt data.
                    txtModel.setColumnIdentifiers(columns);

                    try (BufferedReader br = new BufferedReader(new FileReader(new File("src/main/resources/temp/lists/" + selection + ".txt")))) {

                        while ((line = br.readLine()) != null) {
                            txtModel.addRow(line.split("\t"));
                            table.setModel(txtModel);
                            TableColumnModel colModel = table.getColumnModel();
                            colModel.getColumn(0).setMaxWidth(25);
                            colModel.getColumn(1).setMaxWidth(80);
                            colModel.getColumn(2).setMinWidth(200);
                            table.setSelectionMode(0);
                            DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
                            renderer.setHorizontalAlignment(JLabel.CENTER);
                            table.getColumnModel().getColumn(2).setCellRenderer(renderer);

                        }
                    } catch (FileNotFoundException ex) {
                        java.util.logging.Logger.getLogger(BookShelfUI.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        java.util.logging.Logger.getLogger(BookShelfUI.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        }
    }

    // Exit menu listener.
    class ExitMenu implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            frame.dispose();
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        // TODO code application logic here
        new BookShelfUI();

        DataSource initialSetup = new DataSource();
        initialSetup.getDatabase(new SQLStatements().getDatabaseStatement());   // Initial Database creation.
        LOG.info("Starting Bookshelf.");

    }

}
