/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.bookshelf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yavor
 */

// Show COVER and SYNOPSIS on title selection from combobox.

public class Preview {

    // Get book cover.
    public void getCover(String selected) {

        try (Connection con = new DataSource().getDataSource().getConnection();
                PreparedStatement pst = con.prepareStatement(new SQLStatements().getCoverPreviewStatement())) {
            
            pst.setString(1, selected);
            ResultSet rs = pst.executeQuery();
            
            if (rs.next()) {
                
                // Image file to be read and applied to COVER LABEL;
                File image = new File ("src/main/resources/temp/tempCover.jpg");
                
                // Getting the image file BLOB from MySQL Database;
                try (FileOutputStream fos = new FileOutputStream(image)) {
                    
                    Blob blob = rs.getBlob("Cover");
                    int len = (int) blob.length();
                    
                    byte[] bites = blob.getBytes(1, len);
                    fos.write(bites, 0, len);
                    
                } catch (IOException ex) {
                    Logger.getLogger(Preview.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Preview.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getSynopsis(String selected) {

        try (Connection con = new DataSource().getDataSource().getConnection();
                PreparedStatement pst = con.prepareStatement(new SQLStatements().getSynopsisStatement())) {

            pst.setString(1, selected);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {

                File image = new File ("src/main/resources/temp/tempSynopsis.txt");

                try (FileOutputStream fos = new FileOutputStream(image)) {

                    Blob txt = rs.getBlob("Synopsis");
                    int txtLenhtx = (int) txt.length();
                    byte[] bites = txt.getBytes(1, txtLenhtx);
                    fos.write(bites, 0, txtLenhtx);

                } catch (IOException ex) {
                    Logger.getLogger(Preview.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Preview.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
